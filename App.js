import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import Login from './app/components/login/login';
import MainMenu from './app/components/main_menu/main_menu';

export default function App() {
  return (
    <SafeAreaProvider>
      <View style={styles.container}>
        <MainMenu />
      </View>
    </SafeAreaProvider>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D2D2D2',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
