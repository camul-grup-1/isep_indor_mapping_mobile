import React, { Component, useState } from 'react';
import { View, Text, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image style={styles.image} source={require('../../assets/png/logo.png')} />
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Nº Mecanográfico"
            placeholderTextColor="#003f5c"
          />
        </View>

        <View style={styles.inputView}>
          <TextInput
            style={styles.TextInput}
            placeholder="Password"
            placeholderTextColor="#003f5c"
            secureTextEntry={true}
          />
        </View>

        <View style={styles.btns}>
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.visitanteBtn}>
            <Text style={styles.loginText}>Visitante</Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputView: {
    backgroundColor: "#ffffff",
    borderRadius: 30,
    "border-style": "solid",
    "border-width": 1,
    width: "300px",
    height: 45,
    marginBottom: 20
  },
  image: {
    width: 200,
    height: 200,
    marginBottom: 40
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },

  btns: {
    width: "100%",
    flexDirection:"row"
  },
  btn: {
    width: "45%",
    borderRadius: 25,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 40,
    backgroundColor: "#ffffff",
    borderRadius: 30,
    "border-style": "solid",
    "border-width": 1,
  },
  visitanteBtn: {
    marginLeft: 30,
    width: "45%",
    borderRadius: 25,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 40,
    backgroundColor: "#ffffff",
    borderRadius: 30,
    "border-style": "solid",
    "border-width": 1,
  }
});